@extends('layout.main')

@section('content')

    <main id="main">

        <!-- ======= About Us Section ======= -->
        <section id="about" class="about">
            <div class="container">
                <p>Bali United Football Club is an Indonesian professional club based in Gianyar, Bali. Bali United began operationin 2014 and continues to be of the highest tier in Indonesia football competition, Ligue 1. The club has a vision to grow football industry in Indonesia through creating an ecosystem consisting of 4Cs namely the Club, Community, Corporation and Country. <span style="color: #CA171E; font-weight: bold;">Staying true to this vision</span>, the football club launched a sports agency under the name United creative, to always bring the <span style="color: #CA171E; font-weight: bold;">GAME ON</span> beyon its own Club.</p>
            </div>

            <div class="container" style="padding-top: 10%;">
                <p>Warm regards,
                </p>
            </div>
            <div class="container" style="border-bottom: 1px solid #464444; padding-bottom: 5.5%;">
                <img src="{{asset('assets/images-teltics/v502_1719-cropped.png')}}">
            </div>
        </section><!-- End About Us Section -->

        <!-- ======= Product Section ======= -->
        <section id="product" class="portfolio">
            <div class="container" data-aos="fade-up" data-aos-delay="100">
                
                <div class="row">
                    <div class="col-lg-6 float-left">
                    <p>
                        <span style="color: #101010; font-size: 40px; font-weight: bold;">Product</span><br>
                        <span style="color: #CA171E;">What we can do for you</span>
                    </p>
                    </div>
                    <div class="col-lg-6">
                        <div class="float-right d-none d-lg-block">
                            <button id='btnViewallProduct' class="btn-viewall animate__animated animate__fadeInUp scrollto">VIEW ALL</button>
                        </div>
                    </div>
                </div>

                <div class="row" id='rowProduct' style="padding-top: 5%;">
                    <!-- <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                        <div class="portfolio-wrap">
                            <img src="assets/img/portfolio/portfolio-1.jpg" class="img-fluid" alt="">
                            <div class="portfolio-info">
                                <h4>App 1</h4>
                                <p>App</p>
                                <div class="portfolio-links">
                                    <a href="assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox" title="App 1"><i class="icofont-eye"></i></a>
                                    <a href="portfolio-details.html" title="More Details"><i class="icofont-external-link"></i></a>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>

            </div>
        </section><!-- End Our service Section -->

        <hr style="width: 60%;"></hr>
        <hr style="width: 60%;"></hr>

        <!-- ======= different Section ======= -->
        <section class="portfolio">
            <div class="container" style="border-bottom: 1px solid #464444; padding-bottom: 5.5%;">
                
                <marquee>
                    <div class="row">
                        <div class="col-lg-6 float-left">
                            <h3 style="font-weight: bold; color: #101010;">WHAT MAKE US DIFFERENT</h3>
                        </div>
                        <div class="col-lg-6">
                            <h3 style="font-weight: bold; color: #101010;">WHAT MAKE US DIFFERENT</h3>
                        </div>
                    </div>
                </marquee>

                <div class="owl-carousel events-carousel" data-aos="fade-up" style="padding-top: 50px;">

                    <div class="row event-item">
                        <div class="col-lg-6" style="position: relative;">
                            <img src="assets/images-teltics/v502_1483.png" style="width: 550px !important; height: 350px !important;" class="img-fluid" alt="">
                            <div style="position: absolute; top: 35%; right: 30px; color: white;">
                                <p>
                                    <span style="font-size: 104px; font-style: italic; font-wight: bold;">10.000</span><br>
                                    <span style="position: relative; left: 30px; bottom: 35px;">COMMUNITY LEADER</span>
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-6 pt-4 pt-lg-0 content">
                            <p style="margin-top: 20%;">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
                            </p>
                        </div>
                    </div>

                    <div class="row event-item">
                        <div class="col-lg-6" sty;e="position: relative;">
                            <img src="assets/images-teltics/v502_1483.png" style="width: 550px !important; height: 350px !important;" class="img-fluid" alt="">
                            <div style="position: absolute; top: 35%; right: 30px; color: white;">
                                <p>
                                    <span style="font-size: 104px; font-style: italic; font-wight: bold;">10.000</span><br>
                                    <span style="position: relative; left: 30px; bottom: 35px;">COMMUNITY LEADER</span>
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-6 pt-4 pt-lg-0 content">
                            <p style="margin-top: 20%;">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
                            </p>
                        </div>
                    </div>

                </div>

            </div>
        </section><!-- End different Section -->
        
        <!-- ======= Services Section ======= -->
        <section id="services" class="portfolio">
            <div class="container" data-aos="fade-up" data-aos-delay="100">
                
                <div class="row">
                    <div class="col-lg-6 float-left">
                        <p>
                            <span style="color: #101010; font-size: 40px; font-weight: bold;">Services</span><br>
                            <span style="color: #CA171E;">What we can do for you</span>
                        </p>
                    </div>
                    <div class="col-lg-6">
                        <div class="float-right d-none d-lg-block">
                            <button id="btnViewAllService" class="btn-viewall animate__animated animate__fadeInUp scrollto">VIEW ALL</button>
                        </div>
                    </div>
                </div>

                <div class="row" id='rowServices' style="padding-top: 5%;">
                    
                </div>

            </div>
        </section><!-- End Services Section -->

        <hr style="width: 60%;"></hr>
        <hr style="width: 60%;"></hr>

        <!-- ======= ourwork Section ======= -->
        <section class="portfolio">
            <div class="container" style="border-bottom: 1px solid #464444; padding-bottom: 5.5%;">
                
                <marquee>
                    <div class="row">
                        <div class="col-lg-3 float-left">
                            <h3 style="font-weight: bold; color: #101010;">OUR WORKS</h3>
                        </div>
                        <div class="col-lg-3">
                            <h3 style="font-weight: bold; color: #101010;">OUR WORKS</h3>
                        </div>
                        <div class="col-lg-3 float-left">
                            <h3 style="font-weight: bold; color: #101010;">OUR WORKS</h3>
                        </div>
                        <div class="col-lg-3">
                            <h3 style="font-weight: bold; color: #101010;">OUR WORKS</h3>
                        </div>
                    </div>
                </marquee>

                <div class="owl-carousel testimonials-carousel">

                    <div class="testimonial-item">
                        <img src="assets/images-teltics/v691_2372.png" width="80" style="height: 350px !important;" class="img-fluid" alt="">
                    </div>

                    <div class="testimonial-item">
                        <img src="assets/images-teltics/v733_1558.png" width="80" style="height: 350px !important;" class="img-fluid" alt="">
                    </div>

                    <div class="testimonial-item">
                        <img src="assets/images-teltics/v691_2366.png" width="80" style="height: 350px !important;" class="img-fluid" alt="">
                    </div>

                    <div class="testimonial-item">
                        <img src="assets/images-teltics/Rectangle 118.png" width="80" style="height: 350px !important;" class="img-fluid" alt="">
                    </div>

                </div>

            </div>
        </section><!-- End ourwork Section -->

        <!-- ======= client Section ======= -->
        <section id="services" class="portfolio">
            <div class="container" data-aos="fade-up" data-aos-delay="100">
                
                <div class="row">
                    <div class="col-lg-6 float-left">
                        <p>
                            <span style="color: #101010; font-size: 40px; font-weight: bold;">Clients</span><br>
                            <span style="color: #CA171E;">Our happy client</span>
                        </p>
                    </div>
                    <div class="col-lg-6">
                        <div class="float-right d-none d-lg-block">
                            <!-- <a href="#about" class="btn-viewall animate__animated animate__fadeInUp scrollto">VIEW ALL</a> -->
                        </div>
                    </div>
                </div>

                <!-- ======= Clients Section ======= -->
                <section id="clients" class="clients">
                    <div class="container-fluid" data-aos="zoom-in">
                        <div class="row justify-content-center">
                            <div class="col-xl-10 carousel1">
                                <div class="owl-carousel clients-carousel">
                                    <img src="assets/images-teltics/v502_749.png" alt="">
                                    <img src="assets/images-teltics/v502_744.png" alt="">
                                    <img src="assets/images-teltics/v502_745.png" alt="">
                                    <img src="assets/images-teltics/v502_746.png" alt="">
                                    <img src="assets/images-teltics/v502_747.png" alt="">
                                    <img src="assets/images-teltics/v502_748.png" alt="">
                                    <img src="assets/images-teltics/v502_756.png" alt="">
                                    <img src="assets/images-teltics/v502_751.png" alt="">
                                </div>
                            </div>
                            <div class="col-xl-10 carousel2">
                                <div class="owl-carousel clients-carousel">
                                    <img src="assets/images-teltics/v502_751.png" alt="">
                                    <img src="assets/images-teltics/v502_753.png" alt="">
                                    <img src="assets/images-teltics/v502_759.png" alt="">
                                    <img src="assets/images-teltics/v502_761.png" alt="">
                                    <img src="assets/images-teltics/v502_754.png" alt="">
                                    <img src="assets/images-teltics/v502_742.png" alt="">
                                    <img src="assets/images-teltics/v502_757.png" alt="">
                                    <img src="assets/images-teltics/v502_756.png" alt="">
                                </div>
                            </div>
                            <div class="col-xl-10 carousel3">
                                <div class="owl-carousel clients-carousel">
                                    <img src="assets/images-teltics/v502_753.png" alt="">
                                    <img src="assets/images-teltics/v502_751.png" alt="">
                                    <img src="assets/images-teltics/v502_754.png" alt="">
                                    <img src="assets/images-teltics/v502_759.png" alt="">
                                    <img src="assets/images-teltics/v502_761.png" alt="">
                                    <img src="assets/images-teltics/v502_757.png" alt="">
                                    <img src="assets/images-teltics/v502_742.png" alt="">
                                    <img src="assets/images-teltics/v502_756.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </section><!-- End Clients Section -->

            </div>
        </section><!-- End client Section -->

        <!-- ======= creative project Section ======= -->
        <section class="portfolio" style="position: relative; bottom: 80px;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 float-left" style="text-align: center;">
                        <span style="font-weight: bold; font-style: italic; font-size: 24px">LET'S CREATE</span> <span style="font-weight: bold; font-style: italic; font-size: 24px">CREATIVE PROJECT</span>
                    </div>
                    <div class="col-lg-12" style="text-align: center;">
                        <button  class="btn-viewall animate__animated     animate__fadeInUp scrollto">START PROJECT WITH US</button>
                    </div>
                </div>
            </div>
        </section><!-- creative project Section -->

    </main><!-- End #main -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            // get api product
            var linkApiProduct = "{{ route('apiProducts') }}";
            var linkApiServices = "{{ route('apiServices') }}";
            $.ajax({
                type : "GET",
                cache : false,
                async : true,
                global : false,
                url: linkApiProduct,
                success: function(dataResult){
                    $.each(dataResult, function(key, value){
                        console.log(value)
                        $('#rowProduct').append('<div class="col-lg-4 col-md-6 portfolio-item filter-app rowItemProduct">'+
                    '<div class="portfolio-wrap" style="width: 370px;  height: 240px; position: relative;">'+
                        '<img src="'+value['destination']+'" class="img-fluid">'+
                            '<div style="position: absolute; top: 80%; left: 15px; color: white; width: 100%;">'+
                                '<p>'+
                                    '<span style="font-size: 24px; font-wight: bold; opacity: 70%;">'+value['product_name']+'</span>'+
                                '</p>'+
                            '</div>'+
                    '</div>'+
                '</div>')
                    })
                },
                complete: function(){
                    $(".rowItemProduct").slice(0, 6).show();
                }
	        });

            // get api services
            $.ajax({
                type : "GET",
                cache : false,
                async : true,
                global : false,
                url: linkApiServices,
                success: function(dataResult){
                    $.each(dataResult, function(key, value){
                        console.log(value)
                        $('#rowServices').append('<div class="col-lg-4 col-md-6 portfolio-item filter-app rowItemService">'+
                    '<div class="portfolio-wrap" style="width: 370px;  height: 240px; position: relative;">'+
                        '<img src="'+value['destination']+'" class="img-fluid">'+
                            '<div style="position: absolute; top: 80%; left: 15px; color: white; width: 100%;">'+
                                '<p>'+
                                    '<span style="font-size: 24px; font-wight: bold; opacity: 70%;">'+value['service_name']+'</span>'+
                                '</p>'+
                            '</div>'+
                    '</div>'+
                '</div>')
                    })
                },
                complete: function(){
                    $(".rowItemService").slice(0, 6).show();
                }
	        });
            
        })

        $("#btnViewallProduct").click(function(e){ // click event for load more
            var hiddenLength = $("div.rowItemProduct:hidden").length
            e.preventDefault();
            $("div.rowItemProduct:hidden").slice(0, 10).show(); // select next 10 hidden divs and show them
            if(hiddenLength == 0){ // check if any hidden divs still exist
                alert("No more Product"); // alert if there are none left
            }
        });

        $("#btnViewAllService").click(function(e){ // click event for load more
            var hiddenLength = $("div.rowItemService:hidden").length
            e.preventDefault();
            $("div.rowItemService:hidden").slice(0, 10).show(); // select next 10 hidden divs and show them
            if(hiddenLength == 0){ // check if any hidden divs still exist
                alert("No more Service"); // alert if there are none left
            }
        });

    </script>

@endsection